# Medição Inteligente
![bff](images/api-digi-igua.png)

## Authorization
`Authorization: Bearer <token>`

## Devices
#### `GET` `POST`

Rota para gerenciamento dos dispositivos (smartphones, navegadores) utilizados pelo usuário para acessar a plataforma do Digi Iguá. Toda vez que o usuário acessar em um novo dispositivo, é preciso cadastrá-lo para que possamos enviar as notificações de maneira eficiente.

- **fcm_token `STRING`:** token do dispositivo no Firebase Cloud Messaging


### `GET` list devices from a user
Retorna a lista de dispositivos vinculados ao usuário. A lista pode incluir tokens que representam diferentes celulares e navegadores, por exemplo. Pode ser utilizada para verificar se o usuário está acessando por um novo dispositivo.

**Request**
```json
{}
```
Não possui nenhum parâmero obrigatório. Se for enviado um `fcm_token`, ela verifica se esse dispositivo já está cadastrado no banco de dados. Pega o `user_id`, que é colocado na `req.user` durante o processo de autenticação - ensureAuthenticated.js (middleware).

**Response**
```json
{
    "message": "List devices successfully",
    "data": [
        {
            "id": 106,
            "user_id": 279,
            "fcm_token": "01010101",
            "createdAt": "2022-07-26T18:51:50.000Z",
            "updatedAt": "2022-07-26T18:51:50.000Z"
        },
        {
            "id": 107,
            "user_id": 279,
            "fcm_token": "12391283",
            "createdAt": "2022-07-26T18:52:02.000Z",
            "updatedAt": "2022-07-26T18:52:02.000Z"
        }
    ]
}
```

### POST: create device from a user
Quando o usuário logar em um novo dispositivo, é necessário cadastrá-lo nessa lista.

**Request**
```json
{
  "fcm_token": "c0alNw1PNhzS-JDTafVZY4:APA91bEquwDz54esIWJNj54mCVRPebmlWxKNYUtKvBEzP4DSAhAUDO_lvV2WCqe66Pl0p_cM9Y95IDXxGC3sHu8H9RIRfqKBU12akTkJeOZZzgkOiPDvrKu9-EoPB7fecAgqL2LcNFyj",
}
```
Pega o `user_id`, que é colocado na `req.user` durante o processo de autenticação - ensureAuthenticated.js (middleware).

- **fcm_token:** required

**Response**
```json
{
    "message": "Create device successfully",
    "data": {
        "id": 108,
        "user_id": 279,
        "fcm_token": "c0alNw1PNhzS-JDTafVZY4:APA91bEquwDz54esIWJNj54mCVRPebmlWxKNYUtKvBEzP4DSAhAUDO_lvV2WCqe66Pl0p_cM9Y95IDXxGC3sHu8H9RIRfqKBU12akTkJeOZZzgkOiPDvrKu9-EoPB7fecAgqL2LcNFyj",
        "updatedAt": "2022-08-16T15:38:30.021Z",
        "createdAt": "2022-08-16T15:38:30.021Z"
    }
}
```

## Alerts 
#### `GET` `POST` `PUT` `DELETE`

Rota para configuração e gerenciamento dos alertas personalizados para o consumo de cada usuário.

- **registration_id `INT`:** número da matrícula
- **cubic_meter `FLOAT`:** medida de configuração do consumo em metros cúbicos

### `GET` list alerts from a user
Retorna a lista de alertas do usuário.

**Request**
```json
{}
```
Não possui nenhum parâmero obrigatório. Pega o `user_id`, que é colocado na `req.user` durante o processo de autenticação - ensureAuthenticated.js (middleware).

**Response**
```json
{
    "message": "List alerts successfully",
    "data": [
        {
            "id": 126,
            "registration_id": 2147483647,
            "cubic_meter": 123,
            "createdAt": "2022-08-11T13:52:02.000Z",
            "updatedAt": "2022-08-11T13:52:02.000Z",
            "user_id": 279
        },
        {
            "id": 127,
            "registration_id": 2147483647,
            "cubic_meter": 123,
            "createdAt": "2022-08-11T14:09:16.000Z",
            "updatedAt": "2022-08-11T14:09:16.000Z",
            "user_id": 279
        },
        {
            "id": 128,
            "registration_id": 2147483647,
            "cubic_meter": 123,
            "createdAt": "2022-08-11T16:35:09.000Z",
            "updatedAt": "2022-08-11T16:35:09.000Z",
            "user_id": 279
        }
    ]
}
```

### `POST` create alert from a user
Cadastra um novo alerta para uma matrícula específica do usuário.

**Request**
```json
{
  "registration_id": "347789",
  "cubic_meter": "5"
}
```
Pega o `user_id`, que é colocado na `req.user` durante o processo de autenticação - ensureAuthenticated.js (middleware).

**Response**
```json
{
    "message": "Alert created successfully",
    "data": {
        "id": 132,
        "user_id": 279,
        "registration_id": "347789",
        "cubic_meter": "5",
        "updatedAt": "2022-08-16T15:46:53.885Z",
        "createdAt": "2022-08-16T15:46:53.885Z"
    }
}
```

### `PUT` update alert from a user
Edita um alerta configurado pelo usuário.

**Request**

/api/alerts/**:alertId**
```json
{
  "cubic_meter": "7"
}
```

**Response**
```json
{
    "message": "Update alert successfully",
    "data": {
        "id": 132,
        "registration_id": 347789,
        "cubic_meter": "7",
        "createdAt": "2022-08-16T15:46:53.000Z",
        "updatedAt": "2022-08-16T15:57:21.882Z",
        "user_id": 279
    }
}
```

### `DELETE` delete alert from a user
Delete um alerta configurado pelo usuário.

**Request**

/api/alerts/**:alertId**
```json
{}
```

**Response**
```json
{
    "message": "Delete alert successfully",
    "data": {
        "id": 132,
        "registration_id": 347789,
        "cubic_meter": 7,
        "createdAt": "2022-08-16T15:46:53.000Z",
        "updatedAt": "2022-08-16T15:57:21.000Z",
        "user_id": 279
    }
}
```

## Messages
#### `GET` `POST` `PUT`

Rota para salvar as mensagens para que possamos mostrar o histórico de notificações para o usuário. Pode ser utilizada futuramente para serviços de mensageria externos. O serviço de medição inteligente salvará as mensagens enviadas diretamente no banco de dados.

- **fcm_message_id `STRING`:** identificação da mensagem no Firebase Cloud Messaging
- **is_read `BOOLEAN`:** flag de 'lido' ou 'não lido'
- **title `STRING`:** título da mensagem
- **body `STRING`:** corpo da mensagem
- **registration_id `INT`:** identificação da matrícula


### `GET` list devices from a user
Retorna a lista de dispositivos vinculados ao usuário. A lista pode incluir tokens que representam diferentes celulares e navegadores, por exemplo.

**Request**
```json
{}
```
Não possui nenhum parâmero obrigatório. Pega o `user_id`, que é colocado na `req.user` durante o processo de autenticação - ensureAuthenticated.js (middleware).

**Response**
```json
{
    "message": "List messages successfully.",
    "data": [
        {
            "id": 6,
            "registration_id": 347789,
            "title": "Medição Inteligente",
            "body": "Atenção! Seu consumo de ontem (12/08/2022) ultrapassou 5m³.",
            "fcm_message_id": "388277484729083",
            "is_read": false,
            "createdAt": "2022-08-16T14:00:57.000Z",
            "updatedAt": "2022-08-16T14:00:57.000Z",
            "user_id": 279
        },
        {
            "id": 7,
            "registration_id": 347789,
            "title": "Medição Inteligente",
            "body": "Atenção! Seu consumo de ontem (29/07/2022) ultrapassou 5m³.",
            "fcm_message_id": "222383949430430",
            "is_read": false,
            "createdAt": "2022-08-16T18:41:35.000Z",
            "updatedAt": "2022-08-16T18:41:35.000Z",
            "user_id": 279
        },
        {
            "id": 8,
            "registration_id": null,
            "title": "",
            "body": "Você possui algumas faturas em aberto.",
            "fcm_message_id": "329958688794044",
            "is_read": false,
            "createdAt": "2022-08-16T18:42:34.000Z",
            "updatedAt": "2022-08-16T18:42:34.000Z",
            "user_id": 279
        }
    ]
}
```

### `POST` add message
Quando enviar uma notificação para o usuário, é necessário salvá-la.

**Request**
```json
{
  "fcm_message_id": "3483284389432823",
  "registration_id": 347789,
  "body": "Atenção! Seu consumo de ontem (29/07/2022) ultrapassou 5m³."
}
```
Permite o envio do `user_id` ou pega na `req.user` durante o processo de autenticação - ensureAuthenticated.js (middleware).

- **fcm_message_id:** optional
- **registration_id:** optional
- **user_id:**: optional
- **title:** optional
- **body:** required

**Response**
```json
{
    "message": "Create message successfully.",
    "data": {
        "id": 13,
        "registration_id": 347789,
        "body": "Atenção! Seu consumo de ontem (29/07/2022) ultrapassou 5m³.",
        "fcm_message_id": "3483284389432823",
        "is_read": false,
        "user_id": 123,
        "updatedAt": "2022-08-17T15:12:13.092Z",
        "createdAt": "2022-08-17T15:12:13.092Z"
    }
}
```

### `PUT` update message status
Atualiza o status de lido ou não lido da mensagem.

**Request**

/api/messages/**:messageId**
```json
{
  "is_read": true
}
```

**Response**

```json
{
    "message": "Update message successfully.",
    "data": {
        "id": 10,
        "registration_id": null,
        "title": "",
        "body": "Atenção! Seu consumo de ontem (29/07/2022) ultrapassou 5m³.",
        "fcm_message_id": "12388823948293",
        "is_read": true,
        "createdAt": "2022-08-16T18:43:16.000Z",
        "updatedAt": "2022-08-17T15:05:39.223Z",
        "user_id": 279
    }
}
```